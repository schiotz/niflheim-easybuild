Personal EasyBuild files for Niflheim
=====================================

This repo contains the EasyBuild files for software that I use or
maintain, intended for use on the Niflheim cluster.  Anything useful
beyond that will be contributed back to the EasyBuild projects.
